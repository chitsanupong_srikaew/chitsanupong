﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Test
{
    public class ButtonOff : MonoBehaviour
    {
        public void Exit()
        {
            Application.Quit();
        }
    }

}
